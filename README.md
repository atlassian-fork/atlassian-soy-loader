Atlassian Soy Loader
==============

**This package is deprecated**

- The NPM package was renamed from `@atlassian/atlassian-soy-loader` to [`@atlassian/soy-loader`](http://npmjs.com/package/@atlassian/soy-loader)
- The package was moved to https://bitbucket.org/atlassianlabs/fe-server/src/master/packages/soy-loader